<?php
echo 'Iniciando deploy...<br><br>';

$resultDeploy = [];

try {

	$resultDeploy['message'] = exec('git pull origin sprint', $output);
	$resultDeploy['status_code'] = '200';

} catch() {

	$resultDeploy['message'] = 'Erro ao realizar o deploy';
	$resultDeploy['status_code'] = '400';
	
}

echo 'Retorno: <br>';
echo '<pre>'

print_r(json_encode($resultDeploy));

echo '</pre>'

echo '<br><br> Fim do deploy...';
